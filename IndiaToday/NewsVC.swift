import UIKit
import AVFoundation
import AVKit
import GoogleMobileAds
import MapKit
import CoreLocation
import CoreData
import Alamofire
import SVProgressHUD
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



typealias CompletionHandler = (_ success:Bool,_ res:String) -> Void
typealias NotificationCompletion = (_ success:Bool,_ notificationArr:NSArray) -> Void

class NewsVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate, UINavigationControllerDelegate,MKMapViewDelegate,CLLocationManagerDelegate
{
    //MARK: IBOutlet
    
    @IBOutlet weak var txtPhoneNumber: LetsTextField!
    @IBOutlet weak var viewNoInternet: UIView!
    
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblNoNewsFound: UILabel!
    @IBOutlet weak var txtEmail: LetsTextField!
    @IBOutlet weak var txtName: LetsTextField!
    @IBOutlet weak var txtDescription: LetsTextField!
    @IBOutlet weak var txttTtle: LetsTextField!
    
    @IBOutlet weak var textviewIVitness: UITextView!
    @IBOutlet weak var viewivitenss: UIView!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet var mainvView: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtsearchtext: UITextField!
    @IBOutlet weak var collectionvew: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var scrollviewIvitness: UIScrollView!
    
    
    //MARK: Variables
    let locationManager = CLLocationManager()
    let imagePicker = UIImagePickerController()
    
    var userLatitude:CLLocationDegrees! = 0
    var userLongitude:CLLocationDegrees! = 0
    
    var latituide = Double()
    var longitude = Double()
    var SelectdCatId = "1"
    var headerName = "Top Stories"
    var img:UIImage?
    var langCode = "1"
    var isFirst:Bool = false
    var cell1:NewsDataCell!
    var settingDict:[String:String]?
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewsVC.reload1(_:)), name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: nil)
        delay(0.1000)
        {
            
            if BoolAppStartFirstime == false
            {
                
                BoolAppStartFirstime = true
                if CLLocationManager.locationServicesEnabled()
                {
                    self.locationManager.requestAlwaysAuthorization()
                    self.locationManager.requestWhenInUseAuthorization()
                    self.locationManager.delegate = self
                    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                    self.locationManager.startUpdatingLocation()
                    switch CLLocationManager.authorizationStatus() {
                    case .notDetermined, .restricted, .denied:
                        delay(0.130)
                        {
                            self.LoadNews()
                        }
                        break
                        
                    case .authorizedAlways, .authorizedWhenInUse:
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        self.locationManager.startMonitoringSignificantLocationChanges()
                        self.latituide  = (self.locationManager.location?.coordinate.latitude) ?? 0
                        self.longitude  = (self.locationManager.location?.coordinate.longitude) ?? 0
//                        self.latituide = 23.0225
//                        self.longitude = 72.5714
                        self.GetState(self.latituide, Long: self.longitude)
                        
                        break
                    }
                    
                }
                else
                {
                    delay(0.130)
                    {
                        self.LoadNews()
                        
                    }
                }
                
            }
            if (Default.value(forKey: "Languageid") != nil)
            {
                self.langCode = Default.value(forKey: "Languageid") as! String
            }
            
            self.textviewIVitness.translatesAutoresizingMaskIntoConstraints = true
            self.textviewIVitness.sizeToFit()
            self.textviewIVitness.isScrollEnabled = false
            
           
        }
        
   //      self.laod("http://indianow.beforesubmit.com/api/add_noti_location.php?tUUID=\(appdelegate.deviceToken)&ilanguageCode=\(selectedLangId)&vlat=\(lat)&vlong=\(long)&eDeviceType=2")
        
        imagePicker.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(NewsVC.CatSelected(_:)), name: NSNotification.Name(rawValue: "selectedCategory"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(NewsVC.LangSelected(_:)), name: NSNotification.Name(rawValue: "selectedLang"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewsVC.CloseCat(_:)), name: NSNotification.Name(rawValue: "CloseCategory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewsVC.ChangeTheme(_:)), name: NSNotification.Name(rawValue: "settheme"), object: nil)
        bookmarkTitle = []
//        delay(0.130)
//        {
//               self.LoadNews()
//        }
        
     
    }
    func GetState(_ Lat:Double , Long : Double)  {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: CLLocationDegrees(Lat), longitude: CLLocationDegrees(Long))
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            
            // Place details
            var placeMark: CLPlacemark!
            
            placeMark = placemarks?[0]
            if placeMark != nil
            {
                
                if let administrativeArea = placeMark.addressDictionary!["State"] as? String {
                    
                    print(administrativeArea)
                    self.GetLangaugeID(administrativeArea)
                }
                else
                {
                    delay(0.130)
                    {
                        self.LoadNews()
                    }
                }
                
            }
            else
            {
                delay(0.130)
                {
                    self.LoadNews()
                }
            }
            
        })
    }
    
    
    func GetLangaugeID(_ State:String)
    {
        SVProgressHUD.show(withStatus: "News Loading..")
        
        
        Alamofire.request("http://bcat.org.uk/indianow/api/getLanguageByState.php?state="+State, method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value
            {
                if response.result.isSuccess
                {
                    if ((JSON as AnyObject).value(forKey: "data") != nil && (JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                    {
                        if let data = (JSON as AnyObject).value(forKey: "data") as? [[String : AnyObject]]
                        {
                            if data.count > 0
                            {
                                if let Langaugeid = data[0]["LanguageID"] as? String
                                {
                                    selectedLangId = Langaugeid
                                    
                                    
                                    var selectedLangCode = ""
                                    let LangId = selectedLangId
                                    
                                    if LangId == "1"
                                    {
                                        selectedLangCode = "eng"
                                    }
                                    else if LangId == "2"
                                    {
                                        selectedLangCode = "hi"
                                    }
                                    else if LangId == "3"
                                    {
                                        selectedLangCode = "te"
                                    }
                                    else if LangId == "4"
                                    {
                                        selectedLangCode = "ta"
                                    }
                                    else if LangId == "5"
                                    {
                                        selectedLangCode = "mr"
                                    }
                                    else if LangId == "6"
                                    {
                                        selectedLangCode = "kn"
                                    }
                                    else if LangId == "7"
                                    {
                                        selectedLangCode = "ma"
                                    }
                                    else if LangId == "8"
                                    {
                                        selectedLangCode = "gu"
                                    }
                                    else if LangId == "9"
                                    {
                                        selectedLangCode = "or"
                                    }
                                    else if LangId == "10"
                                    {
                                        selectedLangCode = "bn"
                                    }
                                    else if LangId == "11"
                                    {
                                        selectedLangCode = "gu"
                                    }
                                    else if LangId == "12"
                                    {
                                        selectedLangCode = "as"
                                    }
                                    else
                                    {
                                        selectedLangCode = "eng"
                                    }
                                    NotificationCenter.default.post(name: Notification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: selectedLangCode)
                                    delay(0.130)
                                    {
                                        self.LoadNews()
                                    }
                                }
                            }
                            else
                            {
                                delay(0.130)
                                {
                                    self.LoadNews()
                                }
                            }
                        }
                        else
                        {
                            delay(0.130)
                            {
                                self.LoadNews()
                            }
                        }
                        
                        
                    }
                    else
                    {
                        delay(0.130)
                        {
                            self.LoadNews()
                        }
                    }
                }
                else
                {
                    delay(0.130)
                    {
                        self.LoadNews()
                    }
                }
                
                SVProgressHUD.dismiss()
            }
            else
            {
                delay(0.130)
                {
                    self.LoadNews()
                }
            }

            
        }
        
//        request(.POST,"http://bcat.org.uk/indianow/api/getLanguageByState.php?state="+State, parameters: nil, encoding: .json).responseJSON
//            {
//                response in
//                        }
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        isFirst = true
        delay(0.40)
        {
            self.ChangeTheme1()
        }
        delay(0.33)
        {
           self.BannerAds()
        }
        
        checkMode()
        lblTitle.text = headerName
        collectionvew.reloadData()
        
            }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        let userLocation:CLLocation = locations[0]
        long = "\(userLocation.coordinate.longitude)"
        lat = "\(userLocation.coordinate.latitude)"
    }
    func dismissKeyboard()
    {
        if viewHeader.isHidden == true
        {
            viewHeader.isHidden = false
            if appdelegate.VideoLink != ""
            {
                btnVideo.isHidden = false
            }
            else
            {
                btnVideo.isHidden = false
            }
        }
        else
        {
             viewHeader.isHidden = true
        }
    }
    //MARK:-Custome Function
    @objc func ChangeTheme(_ notification: Notification)
    {
        if theme == "black"
        {
//            btnShare.setImage(UIImage(named: "btn_share"), forState: .Normal)
            
            mainvView.backgroundColor = UIColor.black
        }
        else
        {
//            btnShare.setImage(UIImage(named: "btn_share-1"), forState: .Normal)
            mainvView.backgroundColor = UIColor.white
        }
    }
    func ChangeTheme1()
    {
        if theme == "black"
        {
//            btnShare.setImage(UIImage(named: "btn_share"), forState: .Normal)
            mainvView.backgroundColor = UIColor.black
        }
        else
        {
//            btnShare.setImage(UIImage(named: "btn_share-1"), forState: .Normal)
            mainvView.backgroundColor = UIColor.white
        }
 
    }
    func BannerAds()  {
        self.bannerView.adUnitID = admobBannerId
        self.bannerView.rootViewController = self
        // let request: GADRequest = GADRequest()
        self.bannerView.load(GadRequest())
    }
    
    @objc func LangSelected(_ notification: Notification)
    {
        if !isInternetReachable()
        {
            viewNoInternet.isHidden = false
            //appdelegate.showToast("No Internet Available!!")
            return
        }
        viewNoInternet.isHidden = true
        LoadNews()
    }
    @objc func CatSelected(_ notification: Notification)
    {
        let dict = notification.userInfo! as NSDictionary
        SelectdCatId = String(describing: dict["CatId"]!)
        headerName = String(describing: dict["CatName"]!)
        if !isInternetReachable()
        {
            viewNoInternet.isHidden = false
            //appdelegate.showToast("No Internet Available!!")
            return
        }
        viewNoInternet.isHidden = true
        LoadNews()
    }
    @objc func CloseCat(_ notification: Notification)
    {
        if !isInternetReachable()
        {
            viewNoInternet.isHidden = false
            //appdelegate.showToast("No Internet Available!!")
            return
        }
        viewNoInternet.isHidden = true
        self.collectionvew.reloadData()
    }
   
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        
        return result
    }
    func uploadImageAndData(){
        
        var parameters = [String:AnyObject]()
        parameters = ["":"" as AnyObject]
        
        let URL = "https://indianow.tv/api/data.php?title=\(txttTtle.text!)&desc=\(txtDescription.text!)&email=\(txtEmail.text!)&phone=\(txtPhoneNumber.text!)&name=\(txtName.text!)"
        
        let imageData = self.img!.jpegData(compressionQuality: 0.6)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "file",fileName: "file.png", mimeType: "image/png")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON {
                    response in
                    SVProgressHUD.dismiss()
                    if let JSON = response.result.value {
                        
                        if ((JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                        {
                            self.viewivitenss.isHidden = true
                            self.scrollviewIvitness.isHidden = true
                            let msg = (JSON as AnyObject).value(forKey: "response_message") as! String
                            appdelegate.showToast("\(msg)")
                        }
                        else
                        {
                            appdelegate.showToast("please choose wifi to upload media")
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
        
        
    }
    @objc func reload1(_ notification:Notification){
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "KYDrawerController")
    }

    func LoadNews()
    {
        if SelectdCatId == "5"
        {
//            SelectdCatId = ""
//            let next = storyboard?.instantiateViewController(withIdentifier: "bookmarkVC") as! bookmarkVC
//            navigationController?.pushViewController(next, animated: true)
        }
        else
        {
            clearAll()
            //laod("http://indianow.beforesubmit.com/api/getnews.php?iLanguageID=\(selectedLangId)&iCategoryID=\(SelectdCatId)")
//                        laod("http://indianow.beforesubmit.com/api/getnews.php?iLanguageID=&iCategoryID=\(SelectdCatId)")
          
            laod("https://indianow.tv/api/getnews.php?iLanguageID=\(selectedLangId)&iCategoryID=\(SelectdCatId)")
            
            //laod("https://indianow.tv/api/getnews.php?iLanguageID=1&iCategoryID=1")
           
        }
    }
    func laod(_ url:String)
    {
        SVProgressHUD.show(withStatus: "News Loading..")
        
        Alamofire.request("\(url)", method: .post, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value
            {
                if response.result.isSuccess
                {
                    if ((JSON as AnyObject).value(forKey: "data") != nil && (JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                    {
                        dict = (JSON as AnyObject).value(forKey: "data") as? NSArray
                        vNewsId = dict?.value(forKey: "biNewsID") as? NSArray
                        vNewsTitle = dict?.value(forKey: "vNewsTitle") as? NSArray
                        vNewsDetails = dict?.value(forKey: "tNews_Detail") as? NSArray
                        CreditTo = dict?.value(forKey: "tSourceOfNews") as? NSArray
                        tVideoLink = dict?.value(forKey: "tVideoLink") as? NSArray
                        NewsImages = dict?.value(forKey: "NewsImages") as? NSArray
                        newsReporter = dict?.value(forKey: "vReporterName") as? NSArray
                        newsDesignation = dict?.value(forKey: "vDesignation") as? NSArray
                        newsLocation = dict?.value(forKey: "vLocation") as? NSArray
                        vNewsDate = dict?.value(forKey: "dStartDate") as? NSArray
                        //Default.setObject(dict, forKey: "NewsData")
                        //Default.synchronize()
                        
                    }
                }
                self.collectionvew.reloadData()
                SVProgressHUD.dismiss()
        }
        
        else
        {
            SVProgressHUD.dismiss()
        }
        }
    }
    
    func saveNews()
    {
        let managedContext = AppDelegate().managedObjectContext
        
        let entity = NSEntityDescription.entity(forEntityName: "News", in: managedContext)
        
        let news = NSManagedObject(entity: entity!, insertInto: managedContext)
        
        //results.append(news)
        //let person = NSEntityDescription.insertNewObjectForEntityForName("News", inManagedObjectContext: managedContext)
        
        // add our data
        news.setValue("PMM", forKey: "title")
        
        // save it
        do {
            // this was the problem ///////////////
            try managedContext.save()
            
            
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    var results = [NSManagedObject]()
    func fetchNews()
    {
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "News")
        
        let managedContext = AppDelegate().managedObjectContext
        // Create Entity Description
        //let entityDescription = NSEntityDescription.entityForName("News", inManagedObjectContext: managedContext)
        
        // Configure Fetch Request
        //fetchRequest.entity = entityDescription
        
        do {
            results = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            if (results.count > 0) {
               // let person = result[0] as! NSManagedObject
                
                for title in results
                {
                    print(title.value(forKey: "title")!)
        
                }
               
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    func deleteNews()
    {
        let managedContext = AppDelegate().managedObjectContext
        managedContext.delete(results[0])
        results.remove(at: 0)
        //reload table
    }
    
    func checkMode()
    {
        
        
        settingDict = Default.value(forKey: "settingMode") as? [String : String]
        
        if (Default.value(forKey: "settingMode") != nil)
        {
            if settingDict!["nightMode"] == "on"
            {
                theme = "black"
            }
            else
            {
                theme = "white"
            }
            
        }
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    //MARK:- CollectionView Delegate Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if dict != nil && dict?.count > 0
        {
            lblNoNewsFound.isHidden = true
            return dict!.count
        }
        lblNoNewsFound.isHidden = false
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionvew.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! NewsDataCell
        cell1 = cell
        if bookmarkTitle.contains("\(vNewsTitle![indexPath.row])")
        {
            cell.lblNewtitle.textColor = UIColor.brown
        }
        else
        {
            cell.lblNewtitle.textColor = UIColor.black
        }
        ////
        cell.collectionview.reloadData()

        
        let videoUrl = URL(string: tVideoLink![indexPath.row] as! String)

        if videoUrl != nil
        {
            
            if (tVideoLink![indexPath.row] as! String).range(of:"www.youtube.com") != nil {
                print("yes")
                let urlRequest = URLRequest(url: videoUrl!)
                cell.webviewVideoPlayer.loadRequest(urlRequest)
            }
            else
            {
                var player = AVPlayer()
                
                player = AVPlayer(url: videoUrl! as URL)
                let playerController = AVPlayerViewController()
                playerController.player = player
                self.addChild(playerController)
                
                // Add your view Frame
                playerController.view.frame = cell.webviewVideoPlayer.frame
                
                // Add sub view in your view
                cell.webviewVideoPlayer.addSubview(playerController.view)
                
                //player.play()
                
            }
            
        }
        
        ////
        if tVideoLink![indexPath.row] as! String == ""
        {
            cell.heightConstraintWebview.constant = 0
        }
        else{
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                cell.heightConstraintWebview.constant = 200
            }
            else
            {
                cell.heightConstraintWebview.constant = 300
            }
        }
        cell.images = NewsImages![indexPath.row] as? NSArray
        cell.lblNewtitle.text = vNewsTitle![indexPath.row] as? String
        cell.NewsTitile = vNewsTitle
        cell.VedioLink = tVideoLink
        cell.lblDate.text = vNewsDate![indexPath.row] as? String
        
        let str = (vNewsDetails![indexPath.row] as? String)?.html2String
        
        cell.lblNewsDescription.text = ((str)?.htmlToString)?.replacingOccurrences(of: "\n", with: "\n\n")
        
        cell.lblCreaditTo.text = CreditTo![indexPath.row] as? String
        
        
        
        cell.lblRepNamePlace.text = ((newsReporter![indexPath.row] as? String)! + "/" + (newsDesignation![indexPath.row] as? String)!) + "/" + (newsLocation![indexPath.row] as? String)!

        cell.btnshare.tag = indexPath.row
        cell.btnshare.addTarget(self, action: #selector(btnShareClick(sender:)), for: .touchUpInside)
        cell.shareclick =
            {()->() in
            
//            let textToShare = "\(vNewsTitle![indexPath.row])\n\n\("http://www.indianow.tv/newsdescription.php?nwid=\(vNewsId![indexPath.row])")"
//            print(textToShare)
//            let objectsToShare = [textToShare]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
//            // This lines is for the popover you need to show in iPad
//            activityVC.popoverPresentationController?.sourceView = sender as! UIButton
//
//            // This line remove the arrow of the popover to show in iPad
//           //activityVC.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.
//            //activityVC.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
//            self.present(activityVC, animated: true, completion: nil)
        }
        cell.increase =
            {()->() in
                //Edited
                let currentSize = cell.lblNewsDescription.font.pointSize
                cell.lblNewsDescription.font = cell.lblNewsDescription.font.withSize(CGFloat(currentSize + 1))
                let NewtitleSize = cell.lblNewtitle.font.pointSize
                cell.lblNewtitle.font = cell.lblNewtitle.font.withSize(CGFloat(NewtitleSize + 1))
                let newsReporter = cell.lblRepNamePlace.font.pointSize
                cell.lblRepNamePlace.font = cell.lblRepNamePlace.font.withSize(CGFloat(newsReporter + 1))
                cell.lblNewsDescription.sizeToFit()
        }
        cell.decrease =
            {()->() in
                //Edited
                let currentSize = cell.lblNewsDescription.font.pointSize
                cell.lblNewsDescription.font = cell.lblNewsDescription.font.withSize(CGFloat(currentSize - 1))
                let NewtitleSize = cell.lblNewtitle.font.pointSize
                cell.lblNewtitle.font = cell.lblNewtitle.font.withSize(CGFloat(NewtitleSize - 1))
                let newsReporter = cell.lblRepNamePlace.font.pointSize
                cell.lblRepNamePlace.font = cell.lblRepNamePlace.font.withSize(CGFloat(newsReporter - 1))
                cell.lblNewsDescription.sizeToFit()
        }
        cell.downloadClick =
        {()->() in
            
            
            generalWebService("https://indianow.tv/api/addDownload.php?tDeviceID=\(appdelegate.deviceToken)&iNewsID=\(vNewsId![indexPath.row])&eDeviceType=2", completionHandler: { (success,res) -> Void in
                if success
                {
                     appdelegate.showToast("\(res)")
                }
                else
                {
                    generalWebService("https://indianow.tv/api/deleteDownload.php?tUUID=\(appdelegate.deviceToken)&iNewsID=\(vNewsId![indexPath.row])&eDeviceType=2"
                        , completionHandler: { (success,res) -> Void in
                            if success
                            {
                                appdelegate.showToast("\(res)")
                            }
                            else
                            {
                                appdelegate.showToast("\(res)")
                                
                            }
                    })
                }
            })
        }
        cell.bookmarkClick =
        {()->() in
            generalWebService("https://indianow.tv/api/addbookmark.php?tDeviceID=\(appdelegate.deviceToken)&iNewsID=\(vNewsId![indexPath.row])&eDeviceType=2", completionHandler: { (success,res) -> Void in
                
                if success
                {
                    cell.lblNewtitle.textColor = UIColor.brown
                    appdelegate.showToast("News is Bookmarked!")
                   
                }
                else
                {
                    generalWebService("https://indianow.tv/api/deletebookmark.php?tUUID=\(appdelegate.deviceToken)&iNewsID=\(vNewsId![indexPath.row])"
                        , completionHandler: { (success,res) -> Void in
                            if success
                            {
                                if theme == "white"
                                {
                                    cell.lblNewtitle.textColor = UIColor.black
                                }
                                else
                                {
                                    cell.lblNewtitle.textColor = UIColor.white
                                }
                                appdelegate.showToast("News is Unbookmarked!")
                            }
                            else
                            {
                                cell.lblNewtitle.textColor = UIColor.brown
                                appdelegate.showToast("News is Unbookmarked!")
                            }
                    })
                }
            })
            bookmarkTitle = []
            getNotificationService("https://indianow.tv/api/getbookmark.php?tUUID=\(appdelegate.deviceToken)") { (success, notificationArr) -> Void in
                if success == true
                {
                    bookmarkedNotificationArr = notificationArr
                    for i in 0..<bookmarkedNotificationArr.count 
                    {
                        bookmarkTitle.add("\((bookmarkedNotificationArr[i] as AnyObject).value(forKey: "vNewsTitle")!)")
                    }
                }
            }
        }
        if theme == "white"
        {
            //Edited
            cell.lblNewsDescription.textColor = UIColor.black
            cell.lblNewtitle.textColor = UIColor.black
            cell.lblRepNamePlace.textColor = UIColor.black
            cell.lblDate.textColor = UIColor.black
            cell.lbldate.textColor = UIColor.black
            cell.btnIncrease.setImage(UIImage(named: "zoomin"), for: UIControl.State())
            cell.btnDecrease.setImage(UIImage(named: "zoomout"), for: UIControl.State())
            cell.btnDownload.setImage(UIImage(named: "download"), for: UIControl.State())
            cell.btnshare.setImage(UIImage(named: "btn_share-1"), for: UIControl.State())
            
        }
        else
        {
            //Edited
            cell.lblNewsDescription.textColor = UIColor.white
            cell.lblNewtitle.textColor = UIColor.white
            cell.lblRepNamePlace.textColor = UIColor.white
            cell.lblDate.textColor = UIColor.white
            cell.lbldate.textColor = UIColor.white
            cell.btnIncrease.setImage(UIImage(named: "zoomin-nightmode"), for: UIControl.State())
            cell.btnDecrease.setImage(UIImage(named: "zoomout-nightmode"), for: UIControl.State())
            cell.btnDownload.setImage(UIImage(named: "download-night"), for: UIControl.State())
            cell.btnshare.setImage(UIImage(named: "btn_share"), for: UIControl.State())
           
        }
//        cell.collectionview.reloadData()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        //EDITED
        return CGSize(width: collectionView.frame.size.width,height: collectionView.frame.size.height)
    }
    

    //MARK:- button click event
    
    @objc func btnShareClick(sender:UIButton)
    {
        let textToShare = "\(vNewsTitle![sender.tag])\n\n\("http://www.indianow.tv/newsdescription.php?nwid=\(vNewsId![sender.tag])") \n\nShared via India Now News App: ( Android - indianow.tv/android | iPhone - indianow.tv/iPhone )"
        print(textToShare)
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
        // This lines is for the popover you need to show in iPad
        activityVC.popoverPresentationController?.sourceView = sender
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @IBAction func btnClose_click(_ sender: AnyObject)
    {
        txtsearchtext.text = ""
        viewSearch.isHidden = true
        laod("https://indianow.tv/api/getnews.php?iLanguageID=\(selectedLangId)&iCategoryID=\(SelectdCatId)")
        
        
    }
    @IBAction func btnSearch_click(_ sender: AnyObject)
    {
         laod("https://indianow.tv/api/search.php?q=\(txtsearchtext.text!)")
    }
    @IBAction func btnSearchOpen_click(_ sender: AnyObject)
    {
        viewSearch.isHidden = false
    }
    @IBAction func btnIwitnessClose_click(_ sender: AnyObject)
    {
        viewivitenss.isHidden = true
        scrollviewIvitness.isHidden = true
    }
    
    @IBAction func btnquery_click(_ sender: AnyObject)
    {
        viewivitenss.isHidden = false
        scrollviewIvitness.isHidden = false
    }
    @IBAction func btnMenu_click(_ sender: AnyObject)
    {
        let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
        elDrawer.drawerWidth = self.view.frame.width
        elDrawer.setDrawerState(KYDrawerController.DrawerState.opened, animated: true)
    }
    @IBAction func btnVideo_click(_ sender: AnyObject)
    {
        
        SVProgressHUD.show(withStatus: "Loading..")
        
        Alamofire.request("https://indianow.tv/api/getfrontvideo.php", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value
            {
                print(JSON)
                if response.result.isSuccess
                {
                    if ((JSON as AnyObject).value(forKey: "data") != nil && (JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                    {
                        let dict = (JSON as AnyObject).value(forKey: "data") as? NSDictionary
                        let videoLink = (dict?.value(forKey: "tFrontVideoLink") as? String)!
                        let videoUrl = URL(string: videoLink)
                        
                        if (videoLink).range(of:"www.youtube.com") != nil {
                            print("yes")
                            
                            if let customView = Bundle.main.loadNibNamed("FrontVideoView", owner: self, options: nil)?.first as? FrontVideoView {
                                
                                let urlRequest = URLRequest(url: videoUrl!)
                                customView.webview.loadRequest(urlRequest)
                                
                                customView.frame = self.view.frame
                                
                                self.view.addSubview(customView)
                            }
                        }
                        else
                        {
                            let player = AVPlayer(url: videoUrl!)
                            let playerController = AVPlayerViewController()
                            playerController.player = player
                            
                            self.present(playerController, animated: true) { () -> Void in
                                player.play()
                            }
                            
                        }
                    }
                }
                SVProgressHUD.dismiss()
            }
                
            else
            {
                SVProgressHUD.dismiss()
            }
        }
        
        
    }
    
    @IBAction func btnSend_IWitness_click(_ sender: AnyObject)
    {
        
            if txttTtle.text == ""
            {
                appdelegate.showToast("Please enter title")
                return
            }
            else if txtDescription.text == ""
            {
                appdelegate.showToast("Please enter Description")
                return
            }
            else if txtName.text == ""
            {
                appdelegate.showToast("Please enter Name")
                return
            }
            else if txtEmail.text == ""
            {
                appdelegate.showToast("Please enter Email")
                return
            }
            else if !isValidEmail(txtEmail.text!)
            {
                appdelegate.showToast("Please enter Valid Email")
                return
            }
            else if txtPhoneNumber.text == ""
            {
                appdelegate.showToast("Please enter Phone Number")
                return
            }
            else if img == nil
            {
                appdelegate.showToast("Please select image")
                return
            }
            else
            {
                SVProgressHUD.show(withStatus: "uploading..")
                uploadImageAndData()
            }
        
    }
    
    @IBAction func loadImageButtonTapped(_ sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    

    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            //imageView.contentMode = .ScaleAspectFit
            img = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
