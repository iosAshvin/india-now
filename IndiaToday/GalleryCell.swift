//
//  GalleryCell.swift
//  IndiaToday
//
//  Created by Apple on 09/10/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {
    
    //MARK: IBOutlet
    
    @IBOutlet weak var img: UIImageView!
    
}
