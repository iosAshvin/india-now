//
//  HelpVC.swift
//  IndiaToday
//
//  Created by Apple on 18/12/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class HelpVC: UIViewController {

    //MARK: Variable
    var img:UIImage?
    let imagePicker = UIImagePickerController()
    let arraySex = ["Male","Female"]
    var strSex:String?
    let pickerView = UIPickerView()
    
    //MARK: IBOutlet
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtFatherName: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtSex: UITextField!
    @IBOutlet weak var txtProfession: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }

    //MARK: Function
    
    func setupUI() {
        
        txtSex.inputView = pickerView
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "down-arrow")
        imageView.image = image
        if let size = imageView.image?.size {
            imageView.frame = CGRect(x: 0.0, y: 0.0, width: size.width + 10.0, height: size.height)
        }
        imageView.contentMode = UIView.ContentMode.center
        txtSex.rightView = imageView
        txtSex.rightViewMode = .always
    }
    func API_Call_Submit() {
        
        if txtName.text == ""
        {
            appdelegate.showToast("Please enter name")
            return
        }
        else if txtFatherName.text == ""
        {
            appdelegate.showToast("Please enter father name")
            return
        }
        else if txtAge.text == ""
        {
            appdelegate.showToast("Please enter age")
            return
        }
        else if txtSex.text == ""
        {
            appdelegate.showToast("Please enter sex")
            return
        }
        else if txtProfession.text == ""
        {
            appdelegate.showToast("Please enter profession")
            return
        }
        else if txtMobile.text == ""
        {
            appdelegate.showToast("Please enter mobile")
            return
        }
        else if !isValidEmail(txtEmail.text!)
        {
            appdelegate.showToast("Please enter Valid Email")
            return
        }
        else if txtAddress.text == ""
        {
            appdelegate.showToast("Please enter address")
            return
        }
        else if img == nil
        {
            appdelegate.showToast("Please select image")
            return
        }
        else
        {
            SVProgressHUD.show(withStatus: "uploading..")
            uploadImageAndData()
        }
    }
    
    func uploadImageAndData(){
        
        var parameters = [String:AnyObject]()
        parameters = ["vName":txtName.text!,
                      "vFatherName":txtFatherName.text!,
                      "vAge":txtAge.text!,
                      "eSex":strSex!,
                      "vProfession":txtProfession.text!,
                      "vMobileno":txtMobile.text!,
                      "vEmail":txtEmail.text!,
                      "tAddress":txtAddress.text!] as [String : AnyObject]
        
        let URL = "https://indianow.tv/api/addhelpformdata.php"
        
        let imageData = self.img!.jpegData(compressionQuality: 0.6)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "tImage",fileName: "file.png", mimeType: "image/png")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to:URL)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON {
                    response in
                    SVProgressHUD.dismiss()
                    if let JSON = response.result.value {
                        
                        if ((JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                        {
                            let msg = (JSON as AnyObject).value(forKey: "response_message") as! String
                            appdelegate.showToast("\(msg)")
                            
                            self.txtName.text = ""
                            self.txtFatherName.text = ""
                            self.txtEmail.text = ""
                            self.txtAge.text = ""
                            self.txtProfession.text = ""
                            self.txtSex.text = ""
                            self.txtAddress.text = ""
                            self.txtMobile.text = ""
                            self.img = nil
                        }
                        else
                        {
                            appdelegate.showToast("please choose wifi to upload media")
                        }
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        
        return result
    }
    
    
    //MARK: IBAction
    
    @IBAction func btnSubmit(_ sender: Any) {
        API_Call_Submit()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChoose(_ sender: Any) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

// MARK: - UIImagePickerControllerDelegate Methods

extension HelpVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let pickedImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            //imageView.contentMode = .ScaleAspectFit
            img = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension HelpVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arraySex.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arraySex[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if arraySex[row] == "Male" {
            strSex = "0"
            txtSex.text = "Male"
            
        } else {
            strSex = "1"
            txtSex.text = "Female"
        }
    }
}

extension HelpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            txtFatherName.becomeFirstResponder()
        }
        if textField == txtFatherName {
            txtAge.becomeFirstResponder()
        }
        if textField == txtAge {
            txtSex.becomeFirstResponder()
        }
        if textField == txtSex {
            txtProfession.becomeFirstResponder()
        }
        if textField == txtProfession {
            txtMobile.becomeFirstResponder()
        }
        if textField == txtMobile {
            txtAddress.becomeFirstResponder()
        }
        if textField == txtAddress {
            txtAddress.resignFirstResponder()
        }
        
        return true
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
