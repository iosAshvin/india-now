//
//  ViewController.swift
//  IndiaToday
//
//  Created by DipakChhag on 20/06/16.
//  Copyright © 2016 IndiaToday. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var btnOpen: UIBarButtonItem!

    @IBAction func btnDrawer_Click(_ sender: AnyObject)
    {
        let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
        elDrawer.setDrawerState(KYDrawerController.DrawerState.opened, animated: true)

    }

}

