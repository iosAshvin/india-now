

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var lblCatName: UILabel!
    @IBOutlet weak var imgCatimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
