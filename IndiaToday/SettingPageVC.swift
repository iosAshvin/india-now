import UIKit
import  Alamofire
import SVProgressHUD
import StoreKit


// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

class SettingPageVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var btnMore: UIButton!
    
    @IBOutlet weak var viewfooterline: UIView!
    
    @IBOutlet weak var switchNightMode: UISwitch!
    @IBOutlet weak var switchHdImageMode: UISwitch!
    @IBOutlet weak var switchNotification: UISwitch!
    @IBOutlet weak var lblMore: UILabel!
    @IBOutlet weak var btn_Header_cat_Mode: UIButton!
    @IBOutlet weak var btn_HeaderSetting_Mode: UIButton!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var tblCategoryTable: UITableView!
    @IBOutlet var ScrollView: UIScrollView!
    @IBOutlet var const: NSLayoutConstraint!
    var dict:NSArray = []
    var CatName:NSArray?
    var CatImg:NSArray?
    var CatId:NSArray?
    var LanguageID:NSArray?
    var LangId:String?
    var Language:NSArray?
    var Language_Value:NSArray?
    var cnt = 0
    var langCode:[String] = ["eng","hi","","","ma","gu","","","","","","pa"]
    var settingDict:[String:String]?
    var myview:UIView?
    var toolBar:UIToolbar?
    var name:String!
    var myPickerView : UIPickerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingPageVC.reload1(_:)), name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: nil)
        checkMode()
        LoadCategory()
        getNotificationService("https://indianow.tv/api/getbookmark.php?tUUID=\(appdelegate.deviceToken)") { (success, notificationArr) -> Void in
            if success == true
            {
                bookmarkedNotificationArr = notificationArr as NSArray
                
                
                for i in 0..<bookmarkedNotificationArr.count
                {
                    bookmarkTitle.add("\((bookmarkedNotificationArr[i] as AnyObject).value(forKey: "vNewsTitle")!)")
                }
                
            }
        }
    }
    override func viewWillAppear(_ animated: Bool)
    {
         NotificationCenter.default.addObserver(self, selector: #selector(SettingPageVC.reload1(_:)), name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: nil)
        checkMode()
        LoadCategory()
        getNotificationService("https://indianow.tv/api/getbookmark.php?tUUID=\(appdelegate.deviceToken)") { (success, notificationArr) -> Void in
            if success == true
            {
                bookmarkedNotificationArr = notificationArr as NSArray
                
                
                for i in 0..<bookmarkedNotificationArr.count
                {
                    bookmarkTitle.add("\((bookmarkedNotificationArr[i] as AnyObject).value(forKey: "vNewsTitle")!)")
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        const.constant = ScrollView.contentOffset.x/2
    }
    @IBAction func btn2(_ sender: AnyObject)
    {
        ScrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: 0), animated: true)
    }
    @IBAction func btn1(_ sender: AnyObject)
    {
        ScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    @IBAction func btnCloseCategory_click(_ sender: AnyObject)
    {
        let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
        elDrawer.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
    }
    //MARK:- Table View delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if CatName?.count > 0
        {
            if cnt == 0
            {
                return 5
            }
            else
            {
                 return CatName!.count
            }
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblCategoryTable.dequeueReusableCell(withIdentifier: "Cell")   as! CategoryCell
        
        let url = URL(string: CatImg![indexPath.row] as! String)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            cell.imgCatimage.image = UIImage(data: imageData)
        }
        
        cell.lblCatName.text = CatName![indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (CatName![indexPath.row] as? String) == "Gallery"
        {
            let galleryVC = self.storyboard?.instantiateViewController(withIdentifier: "GalleryViewController") as! GalleryViewController
            self.navigationController?.pushViewController(galleryVC, animated: true)
            return
        }
        else if (CatName![indexPath.row] as? String) == "Bookmarks"
        {
            let bookmarkVC = storyboard?.instantiateViewController(withIdentifier: "bookmarkVC") as! bookmarkVC
            navigationController?.pushViewController(bookmarkVC, animated: true)
            return
        }
        let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
        elDrawer.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
        let catid = CatId![indexPath.row]
        let catname = CatName![indexPath.row]
        let myDict = [ "CatId":catid,"CatName":catname]
        NotificationCenter.default
            .post(name: Notification.Name(rawValue: "selectedCategory"), object: nil, userInfo: myDict)
        
    }
    //MARK:- Setting switch action method
    func SetNotification()
    {}
    //MARK:-Custome Function
    func LoadLanguage()
    {
        if !isInternetReachable()
        {
           // appdelegate.showToast("No Internet Available!!")
            return
        }
        if (Default.value(forKey: "LanguageData") == nil)
        {
            SVProgressHUD.show(withStatus: "Loading..")
            
            Alamofire.request("https://indianow.tv/api/getalllanguage.php", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                if let JSON = response.result.value
                {
                    if response.result.isSuccess
                    {
                        if ((JSON as AnyObject).value(forKey: "data") != nil && (JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                        {
                            self.dict = ((JSON as AnyObject).value(forKey: "data") as? NSArray)!
                            self.LanguageID = self.dict.value(forKey: "iLanguageID") as? NSArray
                            self.Language = self.dict.value(forKey: "vLanguage") as? NSArray
                            self.Language_Value = self.dict.value(forKey: "vLanguage_Value") as? NSArray
                            
                            Default.set(self.dict, forKey: "LanguageData")
                            Default.synchronize()
                            self.tblCategoryTable.reloadData()
                        }
                        else
                        {
                            appdelegate.showToast("Something went wrong. Please try again later.")
                        }
                    }
                }
                
                
            })
            
        }
        else
        {
            self.dict = Default.value(forKey: "LanguageData") as! NSArray
            self.LanguageID = self.dict.value(forKey: "iLanguageID") as? NSArray
            self.Language = self.dict.value(forKey: "vLanguage") as? NSArray
            self.Language_Value = self.dict.value(forKey: "vLanguage_Value") as? NSArray
            self.tblCategoryTable.reloadData()
        }
    }
    func LoadCategory()
    {
        if !isInternetReachable()
        {
            //appdelegate.showToast("No Internet Available!!")
            return
        }
        //if (Default.value(forKey: "CategoryData") == nil)
       // {
            SVProgressHUD.show(withStatus: "Loading categories..")
            
            Alamofire.request("https://indianow.tv/api/getallcategory.php", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { (response) in
                
                SVProgressHUD.dismiss()
                if let JSON = response.result.value
                {
                    print(response)
                    if response.result.isSuccess
                    {
                        self.LoadLanguage()
                        if ((JSON as AnyObject).value(forKey: "data") != nil && (JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                        {
                            self.dict = ((JSON as AnyObject).value(forKey: "data") as? NSArray)!
                            self.CatName = self.dict.value(forKey: "vCategoryName") as? NSArray
                            self.CatId = self.dict.value(forKey: "iCategoryID") as? NSArray
                            self.CatImg = self.dict.value(forKey: "vIosIcon") as? NSArray
                            Default.set(self.dict, forKey: "CategoryData")
                            Default.synchronize()
                            self.tblCategoryTable.reloadData()
                        }
                        else
                        {
                            appdelegate.showToast("Something went wrong. Please try again later.")
                        }
                        print(self.CatImg)
                    }
                }
            })
           
        //}
//        else
//        {
//             self.LoadLanguage()
//            self.dict = (Default.value(forKey: "CategoryData") as? NSArray)!
//            self.CatName = self.dict.value(forKey: "vCategoryName") as? NSArray
//            self.CatId = self.dict.value(forKey: "iCategoryID") as? NSArray
//            self.CatImg = self.dict.value(forKey: "vIosIcon") as? NSArray
//            print(CatImg)
//            self.tblCategoryTable.reloadData()
//        }
    }
    
    
    func openPicker()
    {
        //UIView
        
        myview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        myview?.alpha = 0.5
        myview?.backgroundColor = UIColor.darkGray
        self.view?.addSubview(myview!)
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: self.view.frame.size.height-216, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        // ToolBar
        toolBar = UIToolbar()
        toolBar = UIToolbar(frame:CGRect(x: 0, y: self.view.frame.size.height-256, width: self.view.frame.size.width, height: 40))
        toolBar?.barStyle = .default
        toolBar?.isTranslucent = true
        toolBar?.tintColor = UIColor.darkGray
        toolBar?.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let selectLangButton = UIBarButtonItem(title: "Select Language", style: .plain, target: self, action: nil)
        selectLangButton.isEnabled = false
        let spaceButton2 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar?.setItems([cancelButton, spaceButton, selectLangButton, spaceButton2, doneButton], animated: false)
        
        self.view?.addSubview(self.myPickerView)
        self.view?.addSubview(toolBar!)
    }
    
    func pushnotificationAPICall()
    {
        //SVProgressHUD.show(withStatus: "Loading..")
        
        let finalURL:String = "https://indianow.tv/api/getdevicetoken.php?devicetoken=\(UserDefaults.standard.value(forKey: "deviceToken") ?? "")&devicetype=2" + "&iLanguageID=\(UserDefaults.standard.value(forKey: "Languageid")!)"
        
        
        print(finalURL)
        Alamofire.request("\(finalURL)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value as? [String:Any]
            {
                if JSON["response_status"] as! String == "1"
                {
                    print("Success")
                }
                
               // SVProgressHUD.dismiss()
            }
                
            else
            {
                //SVProgressHUD.dismiss()
            }
        }
    }

    //MARK:- button click events
    @IBAction func HDImage_switch_change(_ sender: AnyObject)
    {
       
        if (Default.value(forKey: "settingMode") != nil)
        {
            if settingDict!["hdImage"] == "on"
            {
                switchHdImageMode.setOn(false, animated: false)
                
                settingDict!["hdImage"] = "off"
            }
            else
            {
                switchHdImageMode.setOn(true, animated: false)
                
                settingDict!["hdImage"] = "on"
            }
            Default.setValue(settingDict, forKey: "settingMode")
            Default.synchronize()
        }
    }
    @IBAction func Notification_switch_change(_ sender: AnyObject)
    {
        if (Default.value(forKey: "settingMode") != nil)
        {
            if settingDict!["notification"] == "on"
            {
                switchNotification.setOn(false, animated: false)
                settingDict!["notification"] = "off"
                
                generalWebService("https://indianow.tv/api/setNotificationFlag.php?tUUID=\(UserDefaults.standard.value(forKey: "deviceToken") ?? "")&eIsNotificationOff=0"
                    , completionHandler: { (success,res) -> Void in
                        if success
                        {
                            //appdelegate.showToast("\(res)")
                        }
                        else
                        {
                            //appdelegate.showToast("\(res)")
                            
                        }
                })
            }
            else
            {
                switchNotification.setOn(true, animated: false)
                settingDict!["notification"] = "on"
                generalWebService("https://indianow.tv/api/setNotificationFlag.php?tUUID=\(UserDefaults.standard.value(forKey: "deviceToken") ?? "")&eIsNotificationOff=1"
                    , completionHandler: { (success,res) -> Void in
                        if success
                        {
                            //appdelegate.showToast("\(res)")
                        }
                        else
                        {
                            //appdelegate.showToast("\(res)")
                            
                        }
                })
            }
            Default.setValue(settingDict, forKey: "settingMode")
            Default.synchronize()
        }
    }
    @IBAction func NightMode_switch_change(_ sender: AnyObject)
    {
        if (Default.value(forKey: "settingMode") != nil)
        {
            if settingDict!["nightMode"] == "on"
            {
                theme = "white"
                switchNightMode.setOn(false, animated: false)
                
                settingDict!["nightMode"] = "off"
            }
            else
            {
                theme = "black"
                switchNightMode.setOn(true, animated: false)
                settingDict!["nightMode"] = "on"
            }
            Default.setValue(settingDict, forKey: "settingMode")
            Default.synchronize()
        }
        NotificationCenter.default.post(name: Notification.Name(rawValue: "settheme"), object: nil)
    }
    @IBAction func btnCategory_click(_ sender: AnyObject)
    {
        openPicker()
    }
    @IBAction func btnFeedback_click(_ sender: AnyObject)
    {
        let next = storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        self.navigationController?.pushViewController(next, animated: true)
    }

    @IBAction func btnSurvey_click(_ sender: Any) {
        
        let next = storyboard?.instantiateViewController(withIdentifier: "SurveyVC") as! SurveyVC
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func btnHelp_click(_ sender: Any) {
        
        let next = storyboard?.instantiateViewController(withIdentifier: "HelpVC") as! HelpVC
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    @IBAction func btnRate_click(_ sender: AnyObject)
    {
//        let appID = "959379869" // Your AppID
//        if let checkURL = URL(string: "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=\(appID)&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8")
//        {
//            if UIApplication.shared.openURL(checkURL)
//            {
//                print("url successfully opened")
//            }
//        } else
//        {
//            print("invalid url")
//        }
        
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        } else {
                // Fallback on earlier versions
        }
        

    }
    @IBAction func btnShare_click(_ sender: AnyObject)
    {
        let textToShare = "Awesome App for news reading"
        if let myWebsite = URL(string: "https://www.indianow.tv/")
        {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            activityVC.popoverPresentationController?.sourceView = sender as? UIView
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnMore_click(_ sender: AnyObject)
    {
        if cnt == 4
        {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.cnt = 0
                self.btnMore.setImage(UIImage.init(named: "ic_cat_more"), for: UIControl.State())
//                ic_cat_more
                self.lblMore.text = "More"
                self.tblCategoryTable.reloadData()
            })
        }
        else
        {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
               self.cnt = 4
                self.btnMore.setImage(UIImage.init(named: "less"), for: UIControl.State())
               self.lblMore.text = "Less"
               self.tblCategoryTable.reloadData()
            })
        }
    }
    @IBAction func btnAboutUs_click(_ sender: AnyObject)
    {
//        let elDrawer: KYDrawerController = (self.navigationController!.parentViewController as! KYDrawerController)
//        elDrawer.setDrawerState(KYDrawerController.DrawerState.Closed, animated: true)
        
        let next = storyboard?.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
        navigationController?.pushViewController(next, animated: true)
        
    }
    @IBAction func btncloseSetting_click(_ sender: AnyObject)
    {
        let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
        elDrawer.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
        
        NotificationCenter.default
            .post(name: Notification.Name(rawValue: "CloseCategory"), object: nil, userInfo: nil)
    }
    @IBAction func btnLanguage_click(_ sender: AnyObject)
    {
        openPicker()
    }
        //MARK:- PickerView Delegate & DataSource
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return Language!.count
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return Language?[row] as? String
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            print(Language!)
            
            self.LangId = self.LanguageID![row] as? String
            print(self.LangId!)
            name = self.Language![row] as! String
            print(name!)
            selectedLangId = self.LangId!
            
        }
    
    
    @objc func doneClick() {
        
        if name != nil
        {
            Default.setValue(self.LangId!, forKey: "Languageid")
            Default.setValue("\(name!)", forKey: "Languagename")
            Default.synchronize()
            
            if UserDefaults.standard.value(forKey: "deviceToken") != nil{
                pushnotificationAPICall()
            }
            
            
            var selectedLangCode = ""
            if self.LangId == "1"
            {
                selectedLangCode = "eng"
            }
            else if self.LangId == "2"
            {
                selectedLangCode = "hi"
            }
            else if self.LangId == "3"
            {
                selectedLangCode = "te"
            }
            else if self.LangId == "4"
            {
                selectedLangCode = "ta"
            }
            else if self.LangId == "5"
            {
                selectedLangCode = "mr"
            }
            else if self.LangId == "6"
            {
                selectedLangCode = "kn"
            }
            else if self.LangId == "7"
            {
                selectedLangCode = "ma"
            }
            else if self.LangId == "8"
            {
                selectedLangCode = "gu"
            }
            else if self.LangId == "9"
            {
                selectedLangCode = "or"
            }
            else if self.LangId == "10"
            {
                selectedLangCode = "bn"
            }
            else if self.LangId == "11"
            {
                selectedLangCode = "gu"
            }
            else if self.LangId == "12"
            {
                selectedLangCode = "as"
            }
            else
            {
                selectedLangCode = "eng"
            }
            self.lblLanguage.text =  name
            //var selectedLangCode = langCode[0]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: selectedLangCode)
            
            let elDrawer: KYDrawerController = (self.navigationController!.parent as! KYDrawerController)
            elDrawer.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
            NotificationCenter.default
                .post(name: Notification.Name(rawValue: "selectedLang"), object: nil, userInfo: nil)
            
            myview?.isHidden = true
            myPickerView.isHidden = true
            toolBar?.isHidden = true
        }
        
    }
    
    @objc func cancelClick() {
        myview?.isHidden = true
        myPickerView.isHidden = true
        toolBar?.isHidden = true
    }
    
    
    func changeToLanguage(_ langCode: String) {
        if Bundle.main.preferredLocalizations.first != langCode
        {
            exit(EXIT_SUCCESS)
        }
    }
    @objc func reload1(_ notification:Notification){
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "KYDrawerController")
    }
    func checkMode()
    {
        
        settingDict = Default.value(forKey: "settingMode") as? [String : String]
        if (Default.value(forKey: "Languageid") != nil)
        {
            selectedLangId =  "\(Default.value(forKey: "Languageid")!)"
            lblLanguage.text = "\(Default.value(forKey: "Languagename")!)"
//            var selectedLangCode = ""
//            if selectedLangId == "1"
//            {
//                selectedLangCode = "eng"
//            }
//            else if selectedLangId == "2"
//            {
//                selectedLangCode = "hi"
//            }
//            else if selectedLangId == "2"
//            {
//                selectedLangCode = "hi"
//            }
//            else if selectedLangId == "5"
//            {
//                selectedLangCode = "mr"
//            }
//            else if selectedLangId == "8"
//            {
//                selectedLangCode = "gu"
//            }
//            else if selectedLangId == "12"
//            {
//                selectedLangCode = "pa"
//            }
//            else
//            {
//                selectedLangCode = "gu"
//            }
       // NSNotificationCenter.defaultCenter().postNotificationName("LANGUAGE_WILL_CHANGE", object: selectedLangCode)
            
        }
        if (Default.value(forKey: "settingMode") != nil)
        {
            if settingDict!["nightMode"] == "on"
            {
                theme = "black"
                switchNightMode.setOn(true, animated: false)
            }
            else
            {
                theme = "white"
                switchNightMode.setOn(false, animated: false)
            }
             NotificationCenter.default.post(name: Notification.Name(rawValue: "settheme"), object: nil)
            if settingDict!["notification"] == "on"
            {
                switchNotification.setOn(true, animated: false)
            }
            else
            {
                switchNotification.setOn(false, animated: false)
            }
            
            if settingDict!["hdImage"] == "on"
            {
                switchHdImageMode.setOn(true, animated: false)
            }
            else
            {
                switchHdImageMode.setOn(false, animated: false)
            }
        }
        
    }
}
