import UIKit

class SettingCell: UITableViewCell
{
    @IBOutlet weak var imgSettingImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var btnSwitch: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
}
