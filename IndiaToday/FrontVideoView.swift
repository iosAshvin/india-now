//
//  FrontVideoView.swift
//  IndiaToday
//
//  Created by Apple on 17/12/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit

class FrontVideoView: UIView {

    //MARK: IBOutlet
    
    @IBOutlet weak var webview: UIWebView!
    
    
    //MARK: IBAction
    
    @IBAction func btnClose(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    

}
