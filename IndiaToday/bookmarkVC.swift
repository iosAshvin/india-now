//
//  bookmarkVC.swift
//  IndiaToday
//
//  Created by DipakChhag on 26/03/17.
//  Copyright © 2017 IndiaToday. All rights reserved.
//

import UIKit
class bookmarkCell: UITableViewCell {
    @IBOutlet weak var lblTitleName: UILabel!
    
    @IBOutlet weak var lblNumber: UILabel!
}

class bookmarkVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    var bookmarkedNotificationArr:NSArray = []
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        LoadData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- custome function 
    func LoadData()
    {
        getNotificationService("https://indianow.tv/api/getbookmark.php?tUUID=\(appdelegate.deviceToken)") { (success, notificationArr) -> Void in
            if success == true
            {
                self.bookmarkedNotificationArr = notificationArr
                
            }
            self.tableview.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if bookmarkedNotificationArr.count > 0
        {
            return bookmarkedNotificationArr.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookmarkCell") as! bookmarkCell
        cell.lblNumber.text = "\(indexPath.row + 1) -"
        cell.lblTitleName.text =  "\((bookmarkedNotificationArr[indexPath.row] as AnyObject).value(forKey: "vNewsTitle")!)"
        return cell
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            let dict = bookmarkedNotificationArr[indexPath.row] as! NSDictionary
            let id = "\(dict.value(forKey: "iNewsID")!)"
            
            generalWebService("https://indianow.tv/api/deletebookmark.php?tUUID=\(appdelegate.deviceToken)&iNewsID=\(id)"
                , completionHandler: { (success,res) -> Void in
                    self.LoadData()
            })
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //var id = "\(bookmarkedNotificationArr[indexPath.row].valueForKey("biNewsID")!)"
    
//        dict = bookmarkedNotificationArr[indexPath.row] as? NSArray 
//        vNewsId = dict?.valueForKey("biNewsID") as? NSArray
//        vNewsTitle = dict?.valueForKey("vNewsTitle") as? NSArray
//        vNewsDetails = dict?.valueForKey("tNews_Detail") as? NSArray
//        CreditTo = dict?.valueForKey("tSourceOfNews") as? NSArray
//        tVideoLink = dict?.valueForKey("tVideoLink") as? NSArray
//        NewsImages = dict?.valueForKey("NewsImages") as? NSArray
//        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func btnBack_click(_ sender: AnyObject)
    {
        navigationController?.popViewController(animated: true)
    }
}
