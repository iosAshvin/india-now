//
//  IntroViewController.swift
//  IndiaToday
//
//  Created by bhikhu vaja on 09/12/17.
//  Copyright © 2017 IndiaToday. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var ObjPageCount: UIPageControl!
    @IBOutlet weak var ObjCollectionview: UICollectionView!
    var items = ["1", "2", "3"]
    var curruntindex  = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        Collectionviewset()

    }
    
    func Collectionviewset()  {
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(IntroViewController.respondToSwipeGesture(_:)))
        swipeLeft.delegate = self
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        ObjCollectionview.addGestureRecognizer(swipeLeft)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        layout.itemSize = CGSize(width: width, height: height)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        ObjCollectionview?.collectionViewLayout = layout
        let nibName = UINib(nibName: "IntroCollectionViewCell", bundle:nil)
        ObjCollectionview.register(nibName, forCellWithReuseIdentifier: "IntroCollectionViewCell")
        ObjCollectionview.reloadData()
    
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool

    {
        return true
    }
   
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
                if  curruntindex == 2  {
                    goToHomePage()
                }
                
            default:
                break
            }
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        
        
        
        if let ip  = ObjCollectionview.indexPathForItem(at: center)
        {
            curruntindex = ip.row
            self.ObjPageCount.currentPage = ip.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.ObjPageCount.numberOfPages = self.items.count
        return self.items.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.ObjCollectionview.frame.size.width , height: self.ObjCollectionview.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
        {
            let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCollectionViewCell", for: indexPath) as! IntroCollectionViewCell
            cell.backgroundColor = UIColor.white
            cell.Objimgview.image = UIImage.init(named: self.items[indexPath.item])
            
        return cell
    }
    
    
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
