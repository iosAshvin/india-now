//
//  AboutUsVC.swift
//  IndiaToday
//
//  Created by DipakChhag on 28/04/17.
//  Copyright © 2017 IndiaToday. All rights reserved.
//

import UIKit

class AboutUsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack_click(_ sender: AnyObject)
    {
        navigationController?.popViewController(animated: true)
    }
}
