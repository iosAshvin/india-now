//
//  AppDelegate.swift
//  IndiaToday
//
//  Created by Ketan Raval on 20/06/16.
//  Copyright © 2016 IndiaToday. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import SVProgressHUD
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire

 var Default = UserDefaults.standard
var appdelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate, UNUserNotificationCenterDelegate {
    var storyboard: UIStoryboard?
    var VideoLink = ""
    var window: UIWindow?
    var deviceToken : String = ""
    var reachability : Reachability!
    let defaults = UserDefaults.standard
     var introductionView: ZWIntroductionViewController?
    var locationManager = CLLocationManager()
    var latitude = 0.0
    var longitude = 0.0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 3))
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.languageWillChange(_:)), name: NSNotification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: nil)
      
        let targetLang = UserDefaults.standard.object(forKey: "selectedLanguage") as? String

        Bundle.setLanguage((targetLang != nil) ? targetLang! : "en")
        self.window?.backgroundColor = UIColor.black
//        NSThread.sleepForTimeInterval(2.0)
        setSettings()
        (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)!
       // print(NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)!)
       
        UIApplication.shared.registerForRemoteNotifications()
        UIApplication.shared.statusBarStyle=UIStatusBarStyle.lightContent
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            // Enable or disable features based on authorization.
        }
        application.isStatusBarHidden = true
        application.registerForRemoteNotifications()
        
        setupReachability(useHostName: true, useClosures: true)
        IQKeyboardManager.shared.enable = true
        SVProgressHUD.setDefaultMaskType(.gradient)
        if (defaults.string(forKey: "isAppAlreadyLaunchedOnce") == nil)
        {
            defaults.setValue(true, forKey: "isAppAlreadyLaunchedOnce")
            setSettings()
            let coverImageNames = ["1","2", "3"]
            //let backgroundImageNames = ["1","2", "3"]
            let enterButton: UIButton? = UIButton()
            enterButton?.setBackgroundImage(UIImage(named: "bg_bar"), for: UIControl.State())
            self.introductionView = ZWIntroductionViewController(coverImageNames: coverImageNames, backgroundImageNames: nil, button: enterButton)
            self.introductionView?.view.backgroundColor = UIColor.white
            self.window = UIWindow()
            self.window!.frame = UIScreen.main.bounds
            self.window?.makeKeyAndVisible()
            self.window?.rootViewController = introductionView
            self.introductionView!.didSelectedEnter =
            {
                self.storyboard = UIStoryboard(name: "Main", bundle: nil)
                self.window?.rootViewController = self.storyboard!.instantiateInitialViewController()
                

            }
            return false
            
//            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
//            let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
//            let initialViewController = storyboard1.instantiateViewControllerWithIdentifier("IntroViewController") as? IntroViewController
//            self.window?.rootViewController = initialViewController
//            self.window?.makeKeyAndVisible()
        }
        else
        {
            
            drawerController = storyBoard.instantiateViewController(withIdentifier: "KYDrawerController") as? KYDrawerController
            drawerController?.drawerWidth = UIScreen.main.bounds.size.width
            appDelegate.window?.rootViewController = drawerController
            
        }
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentationDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        print(documentsPath)
        
        
        // Override point for customization after application launch.
        return true
    }
  
    //MARK: - Push Notification Delegates

    var deviceTokenString:String!
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        
        Default.setValue(deviceTokenString!, forKey: "deviceToken")
        Default.synchronize()
        
        pushnotificationAPICall("https://indianow.tv/api/getdevicetoken.php?devicetoken=\(UserDefaults.standard.value(forKey: "deviceToken") ?? "")&devicetype=2")
        
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        
    }
    
    func pushnotificationAPICall(_ url:String)
    {
        SVProgressHUD.show(withStatus: "Loading..")
        
        var finalURL:String = ""
       
        if UserDefaults.standard.value(forKey: "Languageid") != nil
        {
           finalURL = url + "&iLanguageID=\(UserDefaults.standard.value(forKey: "Languageid")!)"
        }
        else{
            finalURL = url + "&iLanguageID=1"
        }
        
        
        Alamofire.request("\(finalURL)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value as? [String:Any]
            {
                if JSON["response_status"] as! String == "1"
                {
                    print("Success")
                }
                
                SVProgressHUD.dismiss()
            }
                
            else
            {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    //MARK: - Save Device Token
//    func saveDeviceToken(_ deviceToken : NSString)
//    {
//        var deviceTkn = (deviceToken .replacingOccurrences(of: "<", with: ""))
//        
//        deviceTkn = (deviceTkn .replacingOccurrences(of: ">", with: ""))
//        
//        deviceTkn = (deviceTkn .replacingOccurrences(of: "", with: ""))
//        
//        deviceTkn = (deviceTkn .replacingOccurrences(of: " ", with:""))
//        
//        self.deviceToken = deviceTkn
//        print ("Device Token : \(self.deviceToken)")
//        NSLog ("Device Token : %@",self.deviceToken);
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.IndiaToday.IndiaToday" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "IndiaToday", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support
    @objc func languageWillChange(_ notification:Notification)
    {
        let targetLang = notification.object as! String
        UserDefaults.standard.object(forKey: "selectedLanguage") as? String
        Bundle.setLanguage(targetLang)
    
       NotificationCenter.default.post(name: Notification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: nil)
        
        
        //NotificationCenter.Defa.post(name: Notification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: targetLang)
        
    }
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        if let location : CLLocation = manager.location
        {
            self.latitude = location.coordinate.latitude
            self.longitude = location.coordinate.longitude
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error while updating location " + error.localizedDescription)
    }
    func showToast(_ message : String)
    {
        self.window?.makeToast(message, duration: 2.0, position: "center")
    }
    func setupReachability(useHostName: Bool, useClosures: Bool)
    {
        let hostName = "google.com"
        
        do {
            let reachability = try useHostName ? Reachability(hostname: hostName) : Reachability()
            
            self.reachability = reachability
        }
        catch ReachabilityError.FailedToCreateWithAddress(let _)
        {
            return
        } catch {}
        
        if (useClosures)
        {
            self.reachability?.whenReachable = { reachability in
                
            }
            self.reachability?.whenUnreachable = { reachability in
                
            }
        }
    }
    func setSettings()
    {
        if (Default.value(forKey: "settingMode") == nil)
        {
            let settingDict:[String:String] = ["notification":"on","hdImage":"on","nightMode":"off"]
            Default.setValue(settingDict, forKey: "settingMode")
            Default.synchronize()
        }
    }
}
func isInternetReachable()-> Bool
{
    return appdelegate.reachability.isReachable
}
func jsonStringConvert(_ dict : AnyObject) -> String {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
        return String(data: jsonData, encoding: String.Encoding.utf8)! as String
        
    } catch {
        return ""
    }
}
