//
//  SurveyVC.swift
//  IndiaToday
//
//  Created by Apple on 18/12/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import SVProgressHUD

class SurveyVC: UIViewController {

    //MARK: Variable
    
    //MARK: IBOutlet
    
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var webview: WKWebView!
    @IBOutlet weak var constraintWebviewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webview.navigationDelegate = self
        setupUI()
        API_Call_GetSurveyForm()
    }

    func setupUI()
    {
        textview.translatesAutoresizingMaskIntoConstraints = true
        textview.sizeToFit()
        textview.isScrollEnabled = false
        
    }
    
    func API_Call_GetSurveyForm() {
        
        SVProgressHUD.show(withStatus: "Loading..")
        
        let finalURL:String = "https://www.indianow.tv/api/getsurveyform.php"
        
        print(finalURL)
        Alamofire.request("\(finalURL)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value as? [String:Any]
            {
                if JSON["response_status"] as! String == "1"
                {
                    print("Success")
                    
                    let dataDict = JSON["data"] as! [String:String]
                    
                    let myURLString = dataDict["tGoogleFormLink"]
                    let url = URL(string: myURLString!)
                    let request = URLRequest(url: url!)
                    self.webview.load(request)
                }
                SVProgressHUD.dismiss()
            }
            else
            {
                SVProgressHUD.dismiss()
            }
        }
        
    }
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension SurveyVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        SVProgressHUD.show(withStatus: "Loading..")
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        webview.frame.size.height = webview.scrollView.contentSize.height
        webview.frame.size = webview.sizeThatFits(.zero)
        webview.scrollView.isScrollEnabled=false
        constraintWebviewHeight.constant = webview.scrollView.contentSize.height
        SVProgressHUD.dismiss()
    }
    
}
