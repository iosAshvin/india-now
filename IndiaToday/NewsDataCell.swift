
import UIKit
import WebKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

typealias shareclouser = () -> ()
typealias increaseclickn  = () -> ()
typealias decreaseclick  = () -> ()
typealias donwload  = () -> ()
typealias bookmark  = () -> ()
class NewsDataCell: UICollectionViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet weak var btnshare: UIButton!
    @IBOutlet weak var lblCreaditTo: UILabel!
    @IBOutlet weak var lblNewsDescription: UILabel!
    @IBOutlet weak var lblRepNamePlace: UILabel!
    @IBOutlet weak var lblNewtitle: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    
    @IBOutlet weak var webviewVideoPlayer: UIWebView!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var heightConstraintWebview:NSLayoutConstraint!
    
    var VedioLink :NSArray?
    var NewsTitile:NSArray?
    var CreditTo:NSArray?
    var images:NSArray?
    var shareclick:shareclouser?
    var increase:increaseclickn?
    var decrease:decreaseclick?
    var downloadClick:donwload?
    var bookmarkClick:bookmark?

    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnIncrease: UIButton!
    @IBOutlet weak var btnDecrease: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBAction func btnShare_click(_ sender: AnyObject)
    {
        if shareclick != nil
        {
            shareclick!()
        }
    }
    @IBAction func btnIncreaseFont_click(_ sender: AnyObject)
    {
        if increase != nil
        {
            increase!()
        }
    }
    @IBAction func btnDecreseFont_click(_ sender: AnyObject)
    {
        if decrease != nil
        {
            decrease!()
        }
    }
    
    @IBAction func bookmark_Click(_ sender: AnyObject)
    {
        if bookmarkClick != nil
        {
            bookmarkClick!()
        }
    }
    
    @IBAction func download_Click(_ sender: AnyObject)
    {
        if downloadClick != nil
        {
            downloadClick!()
        }
    }
    //MARK:- CollectionView methods
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if images?.count > 0
        {
            return images!.count
        }
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        appdelegate.VideoLink = ""
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "Cell1", for: indexPath) as! NewsImageCell
        
        if images?.count > 0
        {

            appdelegate.VideoLink = VedioLink![indexPath.row] as! String
            let settingDict = Default.value(forKey: "settingMode") as! NSDictionary
            if settingDict.value(forKey: "hdImage")! as! String == "on"
            {
                
                let url = URL(string: images![indexPath.row] as! String)
                let data = try? Data(contentsOf: url!)
                
                if let imageData = data {
                    cell.imgNewsImages.image = UIImage(data: imageData)
                }
                
            }
            else
            {
                //cell.imgNewsImages.image = UIImage(named: "appIcon")
                let url = URL(string: images![indexPath.row] as! String)
                let data = try? Data(contentsOf: url!)
                
                if let imageData = data {
                    cell.imgNewsImages.image = UIImage(data: imageData)?.resized(withPercentage: 0.5)
                }
            }
        }
        else
        {
            cell.imgNewsImages.image = UIImage(named: "placeholder")
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width,height: 300)
    }
   
}
extension UILabel {
    func increaseFontsize ()
    {
        let currentSize = self.font.pointSize
        self.font =  UIFont(name: "", size: CGFloat(currentSize + 1))
        self.sizeToFit()
    }
    func decreaseFontsize ()
    {
        let currentSize = self.font.pointSize
        self.font =  UIFont(name: "", size: CGFloat(currentSize - 1))
        self.sizeToFit()
    }
}
extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }}
