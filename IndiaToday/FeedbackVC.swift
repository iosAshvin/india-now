//
//  FeedbackVC.swift
//  IndiaToday
//
//  Created by Apple on 17/09/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
class FeedbackVC: UIViewController, UITextViewDelegate, MFMailComposeViewControllerDelegate {

    //MARK: IBOutlet
    
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtviewDescription: UITextView!
    @IBOutlet weak var btnSubmit: PJButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtviewDescription.layer.borderColor = UIColor.black.cgColor
        txtviewDescription.layer.borderWidth = 1
    }

    //MARK: IBAction
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        if txtviewDescription.text != "Description..." && txtviewDescription.text != ""
        {
            txtSubject.text = ""
            txtviewDescription.text = "Description..."
            txtviewDescription.textColor = UIColor.lightGray
            
            if MFMailComposeViewController.canSendMail()
            {
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
                composeVC.setToRecipients(["feedback@indianow.tv","ceoindianow@gmail.com"])
                composeVC.setSubject(txtSubject.text ?? "")
                composeVC.setMessageBody(txtviewDescription.text ?? "", isHTML: false)
                self.present(composeVC, animated: true, completion: nil)
            }
        }
        else
        {
            appdelegate.showToast("Please enter Description")
        }
    }
    
    
    @IBAction func btnBeck(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //UITextview Delegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if txtviewDescription.text == "Description..."
        {
            txtviewDescription.text = ""
        }
        txtviewDescription.textColor = UIColor.black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if txtviewDescription.text == ""
        {
            txtviewDescription.text = "Description..."
            txtviewDescription.textColor = UIColor.lightGray
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
