//
//  GalleryViewController.swift
//  IndiaToday
//
//  Created by Apple on 09/10/18.
//  Copyright © 2018 IndiaToday. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class GalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    //MARK: Variables
    
    var imgArr:[String] = []
    
    //MARK: IBOutlet
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var collectionview: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getGalleryImagesAPICall()
    }
    
    //MARK: UICollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "id", for: indexPath) as! GalleryCell
        
        let url = URL(string: imgArr[indexPath.row])
        let data = try? Data(contentsOf: url!)
        cell.img.image = UIImage(data: data!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.imgview.isHidden = false
        self.btnClose.isHidden = false
        self.collectionview.isHidden = true
        let url = URL(string: imgArr[indexPath.row])
        let data = try? Data(contentsOf: url!)
        self.imgview.image = UIImage(data: data!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.collectionview.frame.width-30)/3, height: (self.collectionview.frame.width-30)/3)
    }
    
    //MARK: Function
    
    func getGalleryImagesAPICall()
    {
        SVProgressHUD.show(withStatus: "Loading..")
        
        let finalURL:String = "https://indianow.tv/api/getgallery.php"
        
        
        print(finalURL)
        Alamofire.request("\(finalURL)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if let JSON = response.result.value as? [String:Any]
            {
                if JSON["response_status"] as! String == "1"
                {
                    print("Success")
                    
                    let dataArr = JSON["data"] as! [[String:String]]
                    
                    for i in 0..<dataArr.count
                    {
                        let dataDict = dataArr[i]
                        
                        if UserDefaults.standard.value(forKey: "Languageid") != nil {
                            if (UserDefaults.standard.value(forKey: "Languageid")! as! String) == dataDict["iLanguageID"]
                            {
                                self.imgArr.append(dataDict["tImage"] ?? "")
                            }
                        } else {
                            
                            if (UserDefaults.standard.value(forKey: "Languageid")! as! String) == "1"
                            {
                                self.imgArr.append(dataDict["tImage"] ?? "")
                            } 
                        }
                        
                    }
                }
                self.collectionview.reloadData()
                SVProgressHUD.dismiss()
            }
                
            else
            {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    //MARK: IBAction
    
    @IBAction func btnBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClose(_ sender: Any) {
        
        self.imgview.isHidden = true
        self.btnClose.isHidden = true
        self.collectionview.isHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
