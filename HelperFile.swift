//
//  HelperFile.swift
//  IndiaToday
//
//  Created by DipakChhag on 01/07/16.
//  Copyright © 2016 IndiaToday. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Alamofire
import GoogleMobileAds
import  SVProgressHUD
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
var drawerController : KYDrawerController?


var ObjIntroViewController : IntroViewController?
var change = true
var lat : String  = "23.4512"
var long : String  =  "72.454"
var selectedLangId = "1"
var vNewsTitle:NSArray?
var vNewsId:NSArray?
var vNewsDetails:NSArray?
var CreditTo:NSArray?
var tVideoLink :NSArray?
var NewsImages :NSArray?
var newsReporter: NSArray?
var newsDesignation: NSArray?
var newsLocation: NSArray?
var vNewsDate:NSArray?
var dict :NSArray?
var theme = "white"
var bookmarkTitle:NSMutableArray = []
var BoolAppStartFirstime : Bool = false
var bookmarkedNotificationArr:NSArray = []

var admobBannerId = "ca-app-pub-3387522065813492/5088310732" //Banner ID

struct ConstantsGlobal
{
    static  var LanguageID : String = " "
}

func clearAll()
{
    vNewsTitle = []
    vNewsId = []
    vNewsDetails = []
    CreditTo = []
    tVideoLink = []
    NewsImages = []
    dict = []
}
func goToHomePage()
{
    drawerController = storyBoard.instantiateViewController(withIdentifier: "KYDrawerController") as? KYDrawerController
    drawerController?.drawerWidth = UIScreen.main.bounds.size.width
    appDelegate.window?.rootViewController = drawerController
}
func generalWebService(_ url:String,completionHandler: @escaping CompletionHandler)
{
    
    Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
        if let JSON = response.result.value
        {
            SVProgressHUD.dismiss()
            if response.result.isSuccess
            {
                if ((JSON as AnyObject).value(forKey: "response_status") as? String == "1")
                {
                    completionHandler(true,"\((JSON as AnyObject).value(forKey: "response_message")!)")
                }
                else
                {
                    completionHandler(false,"\((JSON as AnyObject).value(forKey: "response_message")!)")
                }
            }
        }
    }
}
func getNotificationService(_ url:String,NoticompletionHandler: @escaping NotificationCompletion)
{
    SVProgressHUD.show(withStatus: "Loading..")
    
    Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
        
        if let JSON = response.result.value
        {
            var arrNotification:NSArray = []
            if response.result.isSuccess
            {
                SVProgressHUD.dismiss()
                if "\((JSON as AnyObject).value(forKey: "response_status")!)" == "1"
                {
                    arrNotification = ((JSON as AnyObject).value(forKey: "data") as? NSArray)!
                    
                    NoticompletionHandler(true, arrNotification)
                    
                }
                else
                {
                    NoticompletionHandler(true, arrNotification)
                }
            }
        }
    }
    
    
}



func GadRequest()->GADRequest
{
    let request:GADRequest = GADRequest()
    //let devices: [String] = ["fe828c6049558e7466ae0f16fd8d7172",kGADSimulatorID as! String]
    //request.testDevices = devices
    return request
}
func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}
extension String {
    func localized(_ lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }}


